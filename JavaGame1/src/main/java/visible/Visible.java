package visible;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import dto.Jugador;
import dto.Partida;

import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JTable;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

public class Visible extends JFrame {
	private JTextField inputJug1;
	private JTextField inputJug2;

	/**
	 * Launch the application.
	 */
	public static void jugar() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Visible frame = new Visible();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	JLabel lbTurno = new JLabel("Bienvenido!");
	
	JButton btnA1 = new JButton("-");
	JButton btnA2 = new JButton("-");
	JButton btnA3 = new JButton("-");
	JButton btnB1 = new JButton("-");
	JButton btnB2 = new JButton("-");
	JButton btnB3 = new JButton("-");
	JButton btnC1 = new JButton("-");
	JButton btnC2 = new JButton("-");
	JButton btnC3 = new JButton("-");
	
	Partida game = new Partida(true);
	

	
	public Visible() {
		
		setResizable(false);
		setTitle("Tres en ralla");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 646, 364);
		getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 300, 300);
		getContentPane().add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		

		btnA1.setFont(new Font("Tahoma", Font.PLAIN, 50));
		
		GridBagConstraints gbc_btnA1 = new GridBagConstraints();
		gbc_btnA1.fill = GridBagConstraints.BOTH;
		gbc_btnA1.insets = new Insets(0, 0, 5, 5);
		gbc_btnA1.gridx = 0;
		gbc_btnA1.gridy = 0;
		panel.add(btnA1, gbc_btnA1);
		
		btnA2.setFont(new Font("Tahoma", Font.PLAIN, 50));
		GridBagConstraints gbc_btnA2 = new GridBagConstraints();
		gbc_btnA2.fill = GridBagConstraints.BOTH;
		gbc_btnA2.insets = new Insets(0, 0, 5, 5);
		gbc_btnA2.gridx = 1;
		gbc_btnA2.gridy = 0;
		panel.add(btnA2, gbc_btnA2);
		

		btnA3.setFont(new Font("Tahoma", Font.PLAIN, 50));
		GridBagConstraints gbc_btnA3 = new GridBagConstraints();
		gbc_btnA3.fill = GridBagConstraints.BOTH;
		gbc_btnA3.insets = new Insets(0, 0, 5, 0);
		gbc_btnA3.gridx = 2;
		gbc_btnA3.gridy = 0;
		panel.add(btnA3, gbc_btnA3);
		

		btnB1.setFont(new Font("Tahoma", Font.PLAIN, 50));
		GridBagConstraints gbc_btnB1 = new GridBagConstraints();
		gbc_btnB1.fill = GridBagConstraints.BOTH;
		gbc_btnB1.insets = new Insets(0, 0, 5, 5);
		gbc_btnB1.gridx = 0;
		gbc_btnB1.gridy = 1;
		panel.add(btnB1, gbc_btnB1);
		

		btnB2.setFont(new Font("Tahoma", Font.PLAIN, 50));
		GridBagConstraints gbc_btnB2 = new GridBagConstraints();
		gbc_btnB2.fill = GridBagConstraints.BOTH;
		gbc_btnB2.insets = new Insets(0, 0, 5, 5);
		gbc_btnB2.gridx = 1;
		gbc_btnB2.gridy = 1;
		panel.add(btnB2, gbc_btnB2);
		

		btnB3.setFont(new Font("Tahoma", Font.PLAIN, 50));
		GridBagConstraints gbc_btnB3 = new GridBagConstraints();
		gbc_btnB3.fill = GridBagConstraints.BOTH;
		gbc_btnB3.insets = new Insets(0, 0, 5, 0);
		gbc_btnB3.gridx = 2;
		gbc_btnB3.gridy = 1;
		panel.add(btnB3, gbc_btnB3);
		

		btnC1.setFont(new Font("Tahoma", Font.PLAIN, 50));
		GridBagConstraints gbc_btnC1 = new GridBagConstraints();
		gbc_btnC1.fill = GridBagConstraints.BOTH;
		gbc_btnC1.insets = new Insets(0, 0, 0, 5);
		gbc_btnC1.gridx = 0;
		gbc_btnC1.gridy = 2;
		panel.add(btnC1, gbc_btnC1);
		
	
		btnC2.setFont(new Font("Tahoma", Font.PLAIN, 50));
		btnC2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		GridBagConstraints gbc_btnC2 = new GridBagConstraints();
		gbc_btnC2.fill = GridBagConstraints.BOTH;
		gbc_btnC2.insets = new Insets(0, 0, 0, 5);
		gbc_btnC2.gridx = 1;
		gbc_btnC2.gridy = 2;
		panel.add(btnC2, gbc_btnC2);
		

		btnC3.setFont(new Font("Tahoma", Font.PLAIN, 50));
		GridBagConstraints gbc_btnC3 = new GridBagConstraints();
		gbc_btnC3.fill = GridBagConstraints.BOTH;
		gbc_btnC3.gridx = 2;
		gbc_btnC3.gridy = 2;
		panel.add(btnC3, gbc_btnC3);
		
		JButton btnIniciar = new JButton("Nueva Partida");
		
		btnIniciar.setBounds(422, 11, 125, 23);
		getContentPane().add(btnIniciar);
		
		
		lbTurno.setFont(new Font("Bahnschrift", Font.PLAIN, 14));
		lbTurno.setBounds(320, 43, 310, 31);
		getContentPane().add(lbTurno);
		
		JPanel lblJug1 = new JPanel();
		lblJug1.setBounds(320, 85, 310, 121);
		getContentPane().add(lblJug1);
		lblJug1.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Jugador 1:");
		lblNewLabel.setBounds(10, 11, 87, 14);
		lblJug1.add(lblNewLabel);
		
		JLabel lblNmb1 = new JLabel("Nombre:");
		lblNmb1.setBounds(10, 36, 63, 14);
		lblJug1.add(lblNmb1);
		
		inputJug1 = new JTextField();
		inputJug1.setText("Pepe");
		inputJug1.setBounds(83, 36, 217, 20);
		lblJug1.add(inputJug1);
		inputJug1.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Tipo: ");
		lblNewLabel_1.setBounds(10, 78, 63, 14);
		lblJug1.add(lblNewLabel_1);
		
		JRadioButton rdbtnHum1 = new JRadioButton("Humano");
		rdbtnHum1.setSelected(true);
		rdbtnHum1.setBounds(83, 74, 92, 23);
		lblJug1.add(rdbtnHum1);
		
		JRadioButton rdbtnCpu1 = new JRadioButton("CPU");
		rdbtnCpu1.setBounds(193, 74, 92, 23);
		lblJug1.add(rdbtnCpu1);
		
		ButtonGroup grup1 = new ButtonGroup();
		grup1.add(rdbtnCpu1);
		grup1.add(rdbtnHum1);
		
		JPanel lblJug2 = new JPanel();
		lblJug2.setLayout(null);
		lblJug2.setBounds(320, 217, 310, 107);
		getContentPane().add(lblJug2);
		
		JLabel lblNewLabel_2 = new JLabel("Jugador 2:");
		lblNewLabel_2.setBounds(10, 11, 87, 14);
		lblJug2.add(lblNewLabel_2);
		
		JLabel lblNmb1_1 = new JLabel("Nombre:");
		lblNmb1_1.setBounds(10, 36, 63, 14);
		lblJug2.add(lblNmb1_1);
		
		inputJug2 = new JTextField();
		inputJug2.setText("Maquinon");
		inputJug2.setColumns(10);
		inputJug2.setBounds(83, 36, 217, 20);
		lblJug2.add(inputJug2);
		
		JLabel lblNewLabel_1_1 = new JLabel("Tipo: ");
		lblNewLabel_1_1.setBounds(10, 78, 63, 14);
		lblJug2.add(lblNewLabel_1_1);
		
		JRadioButton rdbtnHum2 = new JRadioButton("Humano");
		rdbtnHum2.setBounds(95, 74, 92, 23);
		lblJug2.add(rdbtnHum2);
		
		JRadioButton rdbtnCpu2 = new JRadioButton("CPU");
		rdbtnCpu2.setSelected(true);
		rdbtnCpu2.setBounds(193, 74, 92, 23);
		lblJug2.add(rdbtnCpu2);
		
		ButtonGroup grup2 = new ButtonGroup();
		grup2.add(rdbtnCpu2);
		grup2.add(rdbtnHum2);
		
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean jug1Bot=rdbtnCpu1.isSelected();
				boolean jug2Bot=rdbtnCpu2.isSelected();
				Jugador jug1 =new Jugador(inputJug1.getText(), jug1Bot);
				Jugador jug2 =new Jugador(inputJug2.getText(), jug2Bot);
				game=new Partida(jug1,jug2);
				game.turnoBot();
				if(jug1.isBot() && jug2.isBot()) {
					while(game.getFin()==false) {
						game.turnoBot();
					}
				}
				getPanel();
			}
		});
		
		btnA1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(0, 0, 'O');
				} else {
					game.turno(0, 0, 'X');
				}
				}
				getPanel();
				
				
			}
		});
		btnA2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(0, 1, 'O');
				} else {
					game.turno(0, 1, 'X');
				}
				}
				getPanel();
			}
		});
		btnA3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(0, 2, 'O');
				} else {
					game.turno(0, 2, 'X');
				}
				}
				getPanel();
			}
		});
		btnB1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(1, 0, 'O');
				} else {
					game.turno(1, 0, 'X');
				}
				}
				getPanel();
			}
		});
		btnB2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(1, 1, 'O');
				} else {
					game.turno(1, 1, 'X');
				}
				}
				getPanel();
			}
		});
		btnB3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(1, 2, 'O');
				} else {
					game.turno(1, 2, 'X');
				}
				
				}
				getPanel();
			}
		});
		
		btnC1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(2, 0, 'O');
				} else {
					game.turno(2, 0, 'X');
				}
				}
				getPanel();
			}
		});
		btnC2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(2, 1, 'O');
				} else {
					game.turno(2, 1, 'X');
				}
				}
				getPanel();
				
			}
		});
		btnC3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!fin()) {
				if(game.getTorn()%2==0) {
					game.turno(2, 2, 'O');
				} else {
					game.turno(2, 2, 'X');
				}
				}
				getPanel();
			}
		});
	}
	
	private boolean fin() {
		String ganador;
		if(game.getFin()) {
			if(game.getTorn()%2==0) {
				ganador= game.getJug2().getNombre();
			} else {
				ganador= game.getJug1().getNombre();
			}
			lbTurno.setText("Partida finalitzada el ganador es: "+ganador);
			//System.out.println(game.getTorn());
			
		}
		return game.getFin();
	}
	private void getPanel() {
		if(game.getTorn()!=0) {
			btnA1.setText(""+game.getTaulerValor(0,0));
			btnA2.setText(""+game.getTaulerValor(0,1));
			btnA3.setText(""+game.getTaulerValor(0,2));
			btnB1.setText(""+game.getTaulerValor(1,0));
			btnB2.setText(""+game.getTaulerValor(1,1));
			btnB3.setText(""+game.getTaulerValor(1,2));
			btnC1.setText(""+game.getTaulerValor(2,0));
			btnC2.setText(""+game.getTaulerValor(2,1));
			btnC3.setText(""+game.getTaulerValor(2,2));
			if(game.getTorn()%2==0) {
			lbTurno.setText("Turno num "+game.getTorn()+", te toca "+game.getJug2().getNombre());
			} else {
				lbTurno.setText("Turno num "+game.getTorn()+", te toca "+game.getJug1().getNombre());
			}
			fin();
		}
	}
}
