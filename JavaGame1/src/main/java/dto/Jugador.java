package dto;

public class Jugador {

	private String nombre;
	private boolean bot = false;
	public Jugador(String nombre, boolean bot) {
		this.nombre = nombre;
		this.bot = bot;
	}
	public String getNombre() {
		return nombre;
	}
	public boolean isBot() {
		return bot;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setBot(boolean bot) {
		this.bot = bot;
	}
	
	
	
	
}
