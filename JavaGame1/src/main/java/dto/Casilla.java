package dto;

public class Casilla {

	private char valor;
	private int vida;
	public Casilla() {
		valor='-';
		vida=6;
	}
	public char getValor() {
		return valor;
	}
	public int getVida() {
		return vida;
	}
	public void setValor(char valor) {
		this.valor = valor;
	}
	public void resetVida() {
		this.vida = 6;
	}
	
	public void restVida() {
		if(this.valor!='-') {
		--this.vida;
		}
		if(this.vida==0) {
			resetVida();
			this.valor='-';
		}
	}
	
	
	
	
}
