package dto;

import java.util.Random;

public class Partida {
	private Jugador jug1;
	private Jugador jug2;
	private int torn;
	private Casilla[][] tauler = new Casilla[3][3];
	private boolean finalitzada;
	public Partida(Jugador jug1, Jugador jug2) {
		super();
		this.jug1 = jug1;
		this.jug2 = jug2;
		torn=1;
		tauler=nuevoTablero();
		finalitzada = false;
	}
	
	
	
	public Partida(boolean finalitzada) {
		this.finalitzada = finalitzada;
		this.torn=0;
		this.jug1 = new Jugador("Nadie", false);
		this.jug2 = new Jugador("Nadie", false);
	}



	public Casilla[][] nuevoTablero() {
		Casilla[][] tauler = new Casilla[3][3];
		for(int i=0; i<3;i++) {
			for(int o=0;o<3;o++) {
				tauler[i][o]=new Casilla();
			}
		}
		return tauler;
		
	}

	public boolean turno(int i, int o, char valor) {
		if(this.finalitzada==true) {
			return false;
		} else {
		if(this.tauler[i][o].getValor()=='-') {
			this.tauler[i][o].setValor(valor);
			comprovarVictoria();
			if(!finalitzada) {
			torn++;
			restarVida();
			//escupirTablero();
				turnoBot();
			}
			return true;
		} else {
			return false;
		}
		}
	}
	
	private void restarVida() {
		for(int x=0; x<3;x++) {
			for(int y=0;y<3;y++) {
				tauler[x][y].restVida();
			}
		}
	}
	
	public void turnoBot() {
		if(this.finalitzada==false) {
			if(this.torn%2==0) {
				if(jug2.isBot()) {
					llenarR('O');
					comprovarVictoria();
					if(!finalitzada) {
						torn++;
					}
				}
			} else {
				if(jug1.isBot()) {
					llenarR('X');
					comprovarVictoria();
					if(!finalitzada) {
						torn++;
					}
				}
			}
		} 
		comprovarVictoria();
	}
	
	private void llenarR(char caracter) {
		int x, y;
		boolean llenado=false;
		while(llenado==false) {
			 Random rnd=new Random();
			x= rnd.nextInt(3);
			y=  rnd.nextInt(3);
			//System.out.println(x+" "+y+" " +tauler[x][y].getValor());
			if(tauler[x][y].getValor()=='-') {
				tauler[x][y].setValor(caracter);
				llenado=true;
				restarVida();
				//System.out.println(x+" "+y);
				//escupirTablero();
			} else {
				
			}
		}
	}
	/*
	private void escupirTablero() {
		for(int i=0; i<3;i++) {
			for(int o=0;o<3;o++) {
				System.out.print(tauler[i][o].getValor()+"\t");
			}
			System.out.println();
		}
	}
	*/
	public void comprovarVictoria() {
		
		char i = 'X';
		if(tauler[0][0].getValor() == i && tauler[0][1].getValor() == i &&tauler[0][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[1][0].getValor() == i && tauler[1][1].getValor() == i &&tauler[1][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[2][0].getValor() == i && tauler[2][1].getValor() == i &&tauler[2][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][0].getValor() == i && tauler[1][0].getValor() == i &&tauler[2][0].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][1].getValor() == i && tauler[1][1].getValor() == i &&tauler[2][1].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][2].getValor() == i && tauler[1][2].getValor() == i &&tauler[2][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][0].getValor() == i && tauler[1][1].getValor() == i &&tauler[2][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][2].getValor() == i && tauler[1][1].getValor() == i &&tauler[2][0].getValor() == i ) {
			finalitzada=true;
		} 
		i = 'O';
		if(tauler[0][0].getValor() == i && tauler[0][1].getValor() == i &&tauler[0][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[1][0].getValor() == i && tauler[1][1].getValor() == i &&tauler[1][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[2][0].getValor() == i && tauler[2][1].getValor() == i &&tauler[2][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][0].getValor() == i && tauler[1][0].getValor() == i &&tauler[2][0].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][1].getValor() == i && tauler[1][1].getValor() == i &&tauler[2][1].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][2].getValor() == i && tauler[1][2].getValor() == i &&tauler[2][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][0].getValor() == i && tauler[1][1].getValor() == i &&tauler[2][2].getValor() == i ) {
			finalitzada=true;
		} else if(tauler[0][2].getValor() == i && tauler[1][1].getValor() == i &&tauler[2][0].getValor() == i ) {
			finalitzada=true;
		} 
	}
	//getters and setters
	
	public boolean getFin() {
		return finalitzada;
	}
	public Jugador getJug1() {
		return jug1;
	}

	public Jugador getJug2() {
		return jug2;
	}

	public int getTorn() {
		return torn;
	}

	public char getTaulerValor(int i, int o) {
		return this.tauler[i][o].getValor();
	}

	public void setJug1(Jugador jug1) {
		this.jug1 = jug1;
	}

	public void setJug2(Jugador jug2) {
		this.jug2 = jug2;
	}

	public void setTorn(int torn) {
		this.torn = torn;
	}
	

}
